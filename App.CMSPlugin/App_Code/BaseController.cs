﻿using System.Web.Mvc;
using App.IServices;

/*!
* 文件名称：BaseController父控制器类
* 文件作者：百小僧
* 编写日期：2016-02-23 15:02:04
* 版权所有：百签软件有限公司
* 企业官网：http://www.baisoft.org
* 开源协议：GPL v2 License
* 文件描述：一切从简，只为了更懒！
*/
namespace App.CMSPlugin
{
    public partial class BaseController : Controller
    {
        protected IUserCoreServices _IUserCoreServices;
    }
}